import { hash as coreHash } from 'img-hash-js-core';
import fs from 'fs';
import imageType from 'image-type';
import { PNG } from 'pngjs';
import jpeg from 'jpeg-js';
import bmp from 'bmp-js';
import UTIF from 'utif2';

import { avif, webp } from '@astropub/codecs';

import { getUrlData, isUrl } from './src/url-resolver.js';
import { reduceToRgb } from './src/byte-resolver.js';

export {
  Grayscale, Resize, Dct, Hash, Convert,
} from 'img-hash-js-core';

const extToResolver = {
  png: PNG.sync.read,
  jpg: jpeg.decode,
  bmp: bmp.decode,
  tif: (raw) => {
    const ifds = UTIF.decode(raw);
    UTIF.decodeImage(raw, ifds[0]);
    const rgba = UTIF.toRGBA8(ifds[0]); // Uint8Array with RGBA pixels
    return { data: rgba, width: ifds[0].width, height: ifds[0].height };
  },
  webp: webp.decode,
  avif: avif.decode,
};

// Hash an image!
// Input is a string to a url, local file, etc of an image
// See defaultConfig for options.
export const hash = async (img, userConfig) => {
  const image = await loadBytesFromNode(img);
  const bytes = reduceToRgb(image.data, image.width, image.height);
  return coreHash(bytes, image.width, image.height, userConfig);
};

const loadBytesFromNode = async (img) => {
  const rawFileData = await readRawFileData(img);
  const format = await imageType(rawFileData);

  const resolver = extToResolver[format.ext];
  if (!resolver) {
    throw new Error(`Unknown image format ${format.ext}, try a png instead.`);
  }

  return resolver.call({}, rawFileData);
};

const readRawFileData = async (s) => {
  if (isUrl(s)) {
    return getUrlData(s);
  }

  return fs.readFileSync(s);
};
