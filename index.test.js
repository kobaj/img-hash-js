/* eslint-disable arrow-body-style */
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';

import { hash } from './index.js';

chai.use(chaiAsPromised);
const { expect } = chai;

describe(import.meta.url, () => {
  describe('index', () => {
    const DATA_GRACE = 'src/data/Grace_Hopper.png';
    const DATA_EARTH = 'src/data/As08-16-2593.png';
    const DATA_FOX = 'src/data/Vulpes_vulpes_ssp_fulvus.png';
    const DATA_ANTEP = 'src/data/Antep_erased2.png';

    it('Hashes Grace with defaults', async () => {
      return expect(hash(DATA_GRACE)).to.eventually.equal('jllMjirfzCo=');
    });

    it('Hashes Fox with defaults', async () => {
      return expect(hash(DATA_FOX)).to.eventually.equal('ysfw7Gxq6vE=');
    });

    it('Hashes Earth with defaults', async () => {
      return expect(hash(DATA_EARTH)).to.eventually.equal('aY6PDyc/HkE=');
    });

    it('Hashes Earth from a url with defaults', async () => {
      return expect(hash('https://gitlab.com/kobaj/img-hash-js-core/-/raw/main/src/data/As08-16-2593.png')).to.eventually.equal('aY6PDyc/HkE=');
    });

    it('Hashes (transparent) Antep with defaults', async () => {
      return expect(hash(DATA_ANTEP)).to.eventually.equal('MmBc3IyMngw=');
    });

  });

  describe('formats', () => {
    const DATA = [
      { file: 'src/data/Grace_Hopper.png', expected: 'jllMjirfzCo=' },
      { file: 'src/data/Grace_Hopper.bmp', expected: 'jtjM3nLcjEo=' },
      { file: 'src/data/Grace_Hopper.jpeg', expected: 'jllMjmrdzCo=' },
      { file: 'src/data/Grace_Hopper.webp', expected: 'jllMjirfzCo=' },
      { file: 'src/data/Grace_Hopper.tif', expected: 'jllMjirfzCo=' },
      { file: 'src/data/Grace_Hopper.avif', expected: 'jllMjirfzCo=' },
    ];

    DATA.forEach(({ file, expected }) => {
      it(`Hashes ${file} with defaults`, async () => {
        return expect(hash(file)).to.eventually.equal(expected);
      });
    });
  });
});
