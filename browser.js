import { hash as coreHash } from 'img-hash-js-core';

import { reduceToRgb } from './src/byte-resolver';

export {
  Grayscale, Resize, Dct, Hash, Convert,
} from 'img-hash-js-core';

// Hash an image!
// Input is a string to a url, local file, etc of an image
// See defaultConfig for options.
export const hash = async (img, userConfig) => {
  const image = await loadBytesFromBrowser(img);
  const bytes = reduceToRgb(image.data, image.width, image.height);
  return coreHash(bytes, image.width, image.height, userConfig);
};

const loadBytesFromBrowser = async (img) => {
  const canvas = document.createElement('canvas');
  const context = canvas.getContext('2d');
  const image = await new Promise((r) => { const i = new Image(); i.onload = (() => r(i)); i.src = img; });
  canvas.height = image.naturalHeight;
  canvas.width = image.naturalWidth;
  context.drawImage(image, 0, 0, image.naturalWidth, image.naturalHeight);
  return { data: context.getImageData(0, 0, canvas.width, canvas.height), width: canvas.width, height: canvas.height };
};
