export const reduceToRgb = (data, width, height) => {
  // already rgb
  if (data.length === (width * height * 3)) {
    return data;
  }

  // rgba
  if (data.length === (width * height * 4)) {
    return data.filter((_, i) => (i + 1) % 4);
  }

  // l8
  if (data.length === (width * height)) {
    return new Array(width * height * 3).fill(0).map((_, i) => data[Math.floor(i / 3)]);
  }

  throw new Error(`Unknown data length, expected ${data.length} to be a factor of width * height (${width} * ${height}).`);
};
