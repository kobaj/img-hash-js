/* eslint-disable arrow-body-style */
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';

import { isUrl } from './url-resolver.js';

chai.use(chaiAsPromised);
const { expect } = chai;

describe(import.meta.url, () => {
  describe('url-resolver', () => {
    const DATA = [
      { file: 'src/data/Grace_Hopper.png', expected: false },
      { file: 'src/data', expected: false },
      { file: '/src/data', expected: false },
      { file: 'http://google.com', expected: true },
      { file: 'https://google.com', expected: true },
      { file: 'HTTPS://google.com', expected: true },
      { file: 'c:/users/me', expected: false },
      { file: 'c:\\users\\me', expected: false },
    ];

    DATA.forEach(({ file, expected }) => {
      it(`Determines ${file} ${expected ? 'is' : 'is not'} a url`, async () => {
        return expect(isUrl(file)).to.equal(expected);
      });
    });
  });
});
