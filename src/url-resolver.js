import { URL } from 'url';
import https from 'https';

export const isUrl = (s) => {
  try {
    const url = new URL(s);
    return url.protocol.startsWith('http');
  } catch (err) {
    // Nope.
  }

  return false;
};

export const getUrlData = async (s) => {
  const options = new URL(s);
  const chunks = [];
  return new Promise((r) => {
    https.request(options, (res) => {
      res.on('data', (chunk) => {
        chunks.push(chunk);
      });
      res.on('end', () => r(Buffer.concat(chunks)));
    }).end();
  });
};
